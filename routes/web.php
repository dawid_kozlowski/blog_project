<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
use App\Articles;
use Illuminate\Support\Facades\Input;

Route::get('/testdb', function () {
    $test = new \App\DataResources\CategoriesData();
    $test->getAll();
});


Auth::routes();
Route::get('/', 'HomeController@index')->name('index');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/submit', function () {
    return view('submit');
});

Route::prefix('admin')->middleware('auth')->group(function () {
//    return 'Hello';
    Route::get('', 'Admin\HomeController@index')->name('index');

    Route::get('article/create', 'Admin\ArticlesController@create')->name('create_article');
    Route::get('articles', 'Admin\ArticlesController@index')->name('all_articles');
    Route::get('article/{id}/edit', 'Admin\ArticlesController@edit')->name('edit_article');
    Route::patch('article/{id}', 'Admin\ArticlesController@update')->name('update_article');
    Route::get('article/{id}', 'Admin\ArticlesController@show')->name('show_article');
    Route::delete('article/{id}', 'Admin\ArticlesController@destroy')->name('destroy_article');
    Route::post('article', 'Admin\ArticlesController@store')->name('store_article');

    Route::post('category', 'Admin\CategoriesController@store')->name('store_category');
    Route::get('categories', 'Admin\CategoriesController@index')->name('categories');
    Route::get('category/create', 'Admin\CategoriesController@create')->name('create_category');
    Route::get('category/{id}/edit', 'Admin\CategoriesController@edit')->name('edit_category');
    Route::patch('category/{id}', 'Admin\CategoriesController@update')->name('update_category');
    Route::delete('category/{id}', 'Admin\CategoriesController@destroy')->name('destroy_category');

    Route::get('article/{id}/{slug?}', 'ArticleController@detail');

    Route::delete('img/{gallery}', 'Admin\GalleryController@destroy')->name('destroy_image');
});

Route::resource('articles', 'ArticleController');
Route::get('categories',['uses'=>'CategoryController@manageCategory'])->name('categories_list');
Route::get('/categories/{id}', 'ArticleController@showByCategory')->name('articles_by_category');
Route::get('/tag/{id}', 'ArticleController@showByTag')->name('articles_by_tag');

Route::get('article/{id}/{slug}', 'ArticleController@detail');

Route::post('/search', 'ArticleController@search')->name('search');

//dd(Route::courentRouteName());
Route::get('/{slug?}', function ($slug = null){
    $seoUrl = \App\SeoUrl::where('slug', '=', $slug)->first();
    if ($seoUrl) {
        $request = \Illuminate\Http\Request::create($seoUrl->URL, 'GET');
        return Route::dispatch($request);
    } else {
        abort('404');
    }
});

