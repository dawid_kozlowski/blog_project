<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Mink;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Illuminate\Support\Facades\Auth;
use Laracasts\Behat\Context\Migrator;
use PHPUnit\Framework\Assert;
use Illuminate\Support\Facades\DB;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context, SnippetAcceptingContext
{
    protected $name;
    protected $email;
//    use Migrator;
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When I register :name :email
     */
    public function iRegister($name, $email)
    {
        $this->name =$name;
        $this->email = $email;
        $this->visit('/register');

        $this->fillField('name', $name);
        $this->fillField('email', $email);
        $this->fillField('password', 'password');
        $this->fillField('password_confirmation', 'password');

        $this->pressButton('Register');
    }

    /**
     * @Then I should have an account0
     */
    public function iShouldHaveAnAccount()
    {
        $this->assertSingedIn();
        $this->visit('/logout');
        $this->assertDeleteAccount();


    }
//    ===============================================
    /**
     * @Given I have an account :name :email
     */
    public function iHaveAnAccount($name, $email)
    {
        $this->iRegister($name, $email);
        $this->assertSingedIn();
        $this->visit('logout');
    }

    /**
     * @When I sign in
     */
    public function iSignIn()
    {
        $this->visit('/login');

        $this->fillField('email', $this->email);
        $this->fillField('password', 'password');
        $this->pressButton('Login');
    }

    /**
     * @Then I should be logged in
     */
    public function iShouldBeLoggedIn()
    {
        $this->assertSingedIn();
        $this->visit('logout');
        $this->assertDeleteAccount();
    }
//    =====================================

    /**
     * @Then I open :arg1 link
     */
    public function iOpenLink()
    {
        $this->visit('/admin');
        $this->clickLink('Create Article');
    }

    /**
     * @Then I should be able to add new article with :arg1 :arg2 :arg3 :arg4
     */
    public function iShouldBeAbleToAddNewArticleWith($arg1, $arg2, $arg3, $arg4)
    {
        $this->fillField('article_name', $arg1);
        $this->fillField('article_short_description', $arg1);
        $this->fillField('article_description', $arg1);
//        $this->fillField('seo_url', $arg2);
//        $this->selectOption('article_category', $arg3);
//        $this->fillField('article_tags', $arg4);

        $this->pressButton("Add");

//        $this->showLastResponse();

    }

    /**
     * @Then I should see :arg1 :arg2 :arg3
     */
    public function iShouldSee($arg1, $arg2, $arg3)
    {
//        $this->visit('/admin/articles');
        $this->printCurrentUrl();
        $this->assertSingedIn();


//        $this->clickLink($arg1);

        $this->assertPageContainsText($arg1);
//        $this->assertPageContainsText('By: '.$arg2);
//        $this->assertPageContainsText($arg3);
        $this->assertPageContainsText('Call to a member function getAuthIdentifier() on null');
//        $this->printLastResponse();

        $this->assertDeleteAccount();
    }



//   =====================================
    public function assertSingedIn()
    {
        Assert::assertTrue(Auth::check());
        $this->assertPageAddress('');
    }

    public function assertDeleteAccount()
    {
        return DB::select("
        DELETE FROM users WHERE email = '$this->email'
        ");
    }

    public function assertDeleteArticle()
    {

    }
}
