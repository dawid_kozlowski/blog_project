Feature: New Article

  To be able to add articles
  User
  Adding a new article

  Scenario: Adding New Article
    Given I have an account "John Doe" "john@example.com"
    When I sign in
    Then I open "Create Article" link
    Then I should be able to add new article with "Behat Test" "behat-test" "2" "35"
    Then I should see "Behat Test" "John Doe" "Behat Test"
