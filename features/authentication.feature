Feature: Membership

  In order to give members access
  As an Administrator
  I need authentication and registration for users

  Scenario: Registration
    When I register "John Doe" "john@example.com"
    Then I should have an account0

  Scenario: Successful Authentication
    Given I have an account "John Doe" "john@example.com"
    When I sign in
    Then I should be logged in
