{{--@extends('layouts.app')--}}

{{--@section('categories')--}}

<h1 class="display-4 mt-5 pt-5 poppins">Categories</h1>
<br>
<div class="panel-group categories">
    <div class="panel panel-default">
        @foreach($data['categories'] as $category)
            <div class="list-group-item">

                <a href="{{ route('articles_by_category', $category->id) }}">{{ $category->name }} </a>
                @if(count($category->childs))
                    <a data-toggle="collapse" href="#collapse_{{ $category->id }}"><small><small><i class="glyphicon glyphicon-plus"></i></small></small></a>
                    <span class="badge">{{ count($category->childs) }}</span>
                @endif
            </div>
            @if(count($category->childs))
                <div class="panel-collapse collapse" id="collapse_{{ $category->id }}">
                    <div class="panel-body">
                    @include('manage_child',['childs' => $category->childs])
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>


{{--old version--}}
{{--<nav class="navbar navbar-expand-lg navbar-light bg-light">--}}
{{--    <div class="nav-item dropdown">--}}
{{--        <ul id="navbar-nav mr-auto">--}}
{{--            @foreach($data['categories'] as $category)--}}
{{--                <a href="{{ route('articles_by_category', $category->id) }}">{{ $category->name }}</a>--}}
{{--                @if(count($category->childs))--}}
{{--                    @include('manageChild',['childs' => $category->childs])--}}
{{--                @endif--}}
{{--                <br>--}}
{{--            @endforeach--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</nav>--}}
