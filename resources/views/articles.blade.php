@extends('layouts.app')
@section('navbarOff')
@endsection
@section('content')
    <div class="p-2 col-lg-7 offset-lg-2 col-md-12 offset-md-0 justify-content-center bd-highlight">
        <div class="content">
            <div class="container">
                <div class="card mb-3 m-5 p-5">
                    <div class="d-flex mr-4 ml-4 blog-name">
                        <a href="{{ url('/') }}">
                            <h1 class="font-4rem font-italic">Sic Mundus Creatus Est</h1>
                        </a>
                    </div>
                </div>
                @foreach($articles as $article)
                    @include('articles.article_card')
                @endforeach
            </div>
            {{ $articles->links('layouts.pagination') }}
        </div>
    </div>
@endsection

