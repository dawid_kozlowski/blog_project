@extends('layouts.app')
@section('content')
    @if(isset($articles))
    <div class="p-2 col-7 offset-2 bd-highlight">
        <div class="content">
            <div class="container">
                @foreach($articles as $article)
                    @include('articles.article_card')
                @endforeach
            </div>
            {{ $articles->links('layouts.pagination') }}
        </div>
    </div>
    @endif
@endsection

