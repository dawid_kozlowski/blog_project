<div class="card m-5 p-5">
    <div class="d-flex flex-column flex-lg-row flex-xl-row">
        <div class="col-lg-3 col-md-12 flex-grow-1">
            @foreach($article->images as $image)
                @if($image->status == 'active')
                    <img class="card-img img-circle" src="{{ asset('thumbnail/' . $image->file_name . '.' . $image->file_type) }}" alt="No main img">
                @endif
            @endforeach
        </div>
        <div class="col-lg-9 col-md-12 flex-grow-1">
            <div class="card-body">
                <a class="card-link" href="{{ url('/articles/'.$article->id) }}">
                    <h1 class="card-title font-3rem font-weight-bold">{{ $article->name }}</h1>
                </a>
                <p class="card-text"><small class="h3 text-muted">{{ $article->getLastUpdateDate() }}</small>
                    <small class="font-weight-bold h3">{{ $article->category->name }}</small>
                    <small class="text-muted h3" >{{ rand(1, 50) }} Comments</small>
                </p>
                <p class="card-text h2 poppins">{{ $article->short_description }}</p>
                <a class="card-link h2 font-blue" href="{{ url('/articles/'.$article->id) }}">Read more</a>

            </div>
        </div>
    </div>
</div>
