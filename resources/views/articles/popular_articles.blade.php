<h1 class="display-4 mt-5 pt-5 poppins">Popular articles</h1>
<br>
@foreach($data['popularArticles'] as $popArt)
    <div>
        <div class="card popular-articles-card mb-3" style="max-width: 540px;">
            <div class="row no-gutters">
                <div class="col-md-4">
                    @foreach($popArt->images as $image)
                        @if($image->status == 'active')
                            <img class="card-img popular-article-image" src="{{ asset('thumbnail/' . $image->file_name . '.' . $image->file_type) }}" alt="No main img" >
                        @endif
                    @endforeach
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <a href="{{ url('/articles/'.$popArt->id) }}"><h2>{{$popArt->name}}</h2></a>
                        <p class="card-text popular-article-text"><small class="text-muted h4">
                                <i class="glyphicon glyphicon-calendar"></i>
                                {{ $popArt->getLastUpdateDate() }}
                                <i class="glyphicon glyphicon-user"></i>
                                {{ $popArt->author->name }}</small></p>
                        <p class="card-text popular-article-text"><small class="text-muted"><i class="glyphicon glyphicon-comment"></i> {{ rand(1,50) }}</small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
