@extends('layouts.app')

@section('content')
    <script src="https://cdn.ckeditor.com/ckeditor5/12.3.0/classic/ckeditor.js"></script>
    <div class="card uper" xmlns="http://www.w3.org/1999/html">
        <div class="card-header">
            Add Article
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('articles.store') }}">
                <div class="form-group">
                    @csrf
                    <label for="article_name">Title:</label>
                    <input type="text" class="form-control" name="article_name"/>
                </div>
                <div class="form-group">
                    <label for="article_short_description">Short description:</label>
                    <input type="text" class="form-control" name="article_short_description"/>
                </div>
                <div class="form-group">
                    <label for="article_description">Description:</label>
                    <textarea name="article_description" id="editor"></textarea>
                </div>
                <div class="form-group">
                    <label for="article_category">Category:</label>
                    <select class="custom-select" name="article_category" id="article_category">
                        @foreach($allCategories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="article_tags">Tags:</label>
                    <select class="custom-select" name="article_tags[]" id="article_tags" multiple>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">#{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
        <script>
            $(document).ready(function () {
                ClassicEditor
                    .create( document.querySelector( '#editor' ) )
                    .catch( error => {
                        console.error( error );
                    } );
            })
        </script>
    </div>
@endsection
