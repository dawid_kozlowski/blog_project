@extends('layouts.app')
@section('navbarOff')
@endsection
@section('content')
    <div class="p-2 col-lg-2 col-xl-2 offset-lg-1 offset-xl-1 offset-0 col-12 flex-grow-1 bd-highlight mt-auto mb-auto article-content">
        <div class="d-flex flex-column">
            <div class="p-2 bd-highlight">
                <h1 class="mb-5">Gallery</h1>
            </div>
            @foreach($gallery as $img)
                <div class="p-2 bd-highlight">
                    <img class="img-circle mb-5 mt-5" src="{{ asset('thumbnail/' . $img->file_name . '.' . $img->file_type) }}" alt="No main img">
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-1 border-left d-none d-lg-block d-xl-block"></div>
    <div class="p-2 col-lg-5 col-xl-5 col-12 flex-grow-1 bd-highlight ml-auto mr-5 article-content">
        <h1 class="display-3">{{ $article->article_name }}</h1>
        @if($image)
            <img src="{{ asset('thumbnail/' . $image->file_name . '.' . $image->file_type) }}" alt="No main img">
        @endif
        <p>{!! $article->description !!}</p>
        <p>
            @foreach($tags as $tag)
                <small><a class="" href="{{ route('articles_by_tag', ['id' => $tag->tag_id]) }}">{{ $tag->name }}</a></small>
            @endforeach
        </p>
{{--            <a href="{{ action('ArticleController@edit', $article->article_id) }}" class="btn btn-secondary">Edit</a>--}}
{{--            <form action="{{ action('ArticleController@destroy', $article->article_id) }}" method="POST">--}}
{{--                <input type="hidden" name="_method" value="DELETE">--}}
{{--                <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                <input type="submit" class="btn btn-danger" value="Delete"/>--}}
{{--            </form>--}}
    </div>
@endsection
