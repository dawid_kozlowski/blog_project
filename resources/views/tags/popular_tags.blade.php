<h1 class="display-4 mt-5 pt-5 poppins">Tag cloud</h1>
<p class="text-justify poppins">
    {{--                        TODO: size of tags depending on the quantity you use--}}
    @foreach($data['tags'] as $tag)
        <a class="poppins border h3 text-uppercase rounded pr-2 pl-2 pt-1 pb-1" href="{{ route('articles_by_tag', ['id' => $tag->tag->id]) }}">{{ $tag->tag->name }}</a>
    @endforeach
</p>
