@extends('admin.layouts.app')

@section('content')
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="nav-item dropdown">
                @foreach($categories as $category)
                <ul id="navbar-nav mr-auto">
                    <a class="{{ ($category->active == 0)?('text-danger'):''}}" href="">{{ $category->name }}</a>
                    <a href="{{ route('edit_category', $category->id) }}" class="btn btn-primary btn-sm">Edit</a>
                    @if(count($category->childs))
                        @include('admin.categories.manage_child',['childs' => $category->childs])
                    @endif
                </ul>
            @endforeach
        </div>
    </nav>
@endsection
