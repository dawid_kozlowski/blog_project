@extends('admin.layouts.app')

@section('content')
    <div class="card uper" xmlns="http://www.w3.org/1999/html">
        <div class="card-header">
            Edit Category
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('update_category', $category->id) }}">
                @method('PATCH')
                <div class="form-group">
                    @csrf
                    <label for="category_name">Category name:</label>
                    <input type="text" class="form-control" name="category_name" value="{{ $category->name }}"/>
                </div>
                <div class="form-group">
                    <label for="category_description">Description:</label>
                    <input type="text" class="form-control" name="category_description" value="{{ $category->description }}"/>
                </div>
                <div class="form-group">
                    <label for="category_active">Is active:</label>
                    <input type="checkbox" name="category_active" value="category_active"
                           {{--                           {{ ($article->active?'checked':'') }}--}}
                           @if($category->active) checked @endif>
                </div>
                <div class="form-group">
                    <label for="category_description">Slug URL:</label>
                    <input type="text" class="form-control" name="seo_url" value="@if($seoUrl != null){{ $seoUrl->slug }} @endif">
                </div>
                <div class="form-group">
                    <label for="parent_category">Parent category:</label>
                    <select class="custom-select" name="parent_category" id="parent_category">
                        <option value="0"
                                @if($category->parent_id == '0')
                                selected
                                @endif
                        >No Parent category</option>
                        @foreach($allCategories as $allCategory)
                            <option value="{{ $allCategory->id }}"
                            @if($allCategory->id == $category->parent_id && $category->parent_id != 0)
                                selected
                            @endif
                            >{{ $allCategory->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
                <form action="{{ action('Admin\CategoriesController@destroy', $category->id) }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" class="btn btn-danger" value="Delete"/>
                </form>
        </div>
    </div>
@endsection
