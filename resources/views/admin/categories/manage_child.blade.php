<ul>
    @foreach($childs as $child)
        <li>
        <a class="{{ ($child->active == 0)?('text-danger'):''}}" href="">{{ $child->name }}</a>
                <a class="btn btn-primary btn-sm" href="{{ route('edit_category', $child->id) }}">Edit</a>
            @if(count($child->childs))
                @include('admin.categories.manage_child',['childs' => $child->childs])
            @endif
        </li>
    @endforeach
</ul>
