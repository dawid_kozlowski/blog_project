@extends('admin.layouts.app')

@section('content')
    <div class="card uper" xmlns="http://www.w3.org/1999/html">
        <div class="card-header">
            Add Category
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('store_category') }}">
                <div class="form-group">
                    @csrf
                    <label for="category_name">Category name:</label>
                    <input type="text" class="form-control" name="category_name"/>
                </div>
                <div class="form-group">
                    <label for="category_description">Description:</label>
                    <input type="text" class="form-control" name="category_description"/>
                </div>
                <div class="form-group">
                    <label for="category_description">Slug URL:</label>
                    <input type="text" class="form-control" name="seo_url">
                </div>
                <div class="form-group">
                    <label for="parent_category">Parent category:</label>
                    <select class="custom-select" name="parent_category" id="parent_category">
                        <option value="0">No Parent category</option>
                        @foreach($allCategories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
@endsection
