@extends('admin.layouts.app')

@section('content')
    <script src="https://cdn.ckeditor.com/ckeditor5/12.3.0/classic/ckeditor.js"></script>
    <script src="{{ asset('assets/select-2/js/select2.js') }}"></script>
    <div class="card uper" xmlns="http://www.w3.org/1999/html">
        <div class="card-header">
            Add Article
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('store_article') }}" enctype="multipart/form-data">
                <div class="form-group">
                    @csrf
                    <label for="article_name">Title:</label>
                    <input type="text" class="form-control" name="article_name"/>
                </div>
                <div class="form-group">
                    <label for="article_short_description">Short description:</label>
                    <input type="text" class="form-control" name="article_short_description"/>
                </div>
                <div class="form-group">
                    <label for="article_description">Description:</label>
                    <textarea name="article_description" id="editor"></textarea>
                </div>
                <div class="form-group">
                    <label for="article_active">Is active:</label>
                    <input type="checkbox" name="article_active" value="article_active" checked>
                </div>

                <div class="input-group control-group increment" >
                    <input type="file" name="image[]" id="fileToUpload">
                    <div class="input-group-btn">
                        <button class="btn btn-success" type="button"><i class="icon icon-plus"></i>Add Image</button>
                    </div>
                </div>
                <div class="clone hide" hidden>
                    <div class="control-group input-group" style="margin-top:10px">
                        <input type="file" name="image[]" id="fileToUpload">
                        <div class="input-group-btn">
                            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                        </div>
                    </div>
                </div>

                <label for="category_description">Slug URL:</label>
                <input type="text" class="form-control" name="seo_url">
                <div class="form-group">
                    <label for="article_category">Category:</label>
                    <select class="custom-select" name="article_category" id="article_category">
                        @foreach($allCategories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="article_tags">Tags:</label>
                    <select class="form-control" name="article_tags[]" id="article_tags" multiple>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
                <input type="hidden" name="new_tags">
            </form>
        </div>
        <script>
            $(document).ready(function () {
                ClassicEditor
                    .create( document.querySelector( '#editor' ) )
                    .catch( error => {
                        console.error( error );
                    } );
                $("#article_tags").select2({
                    tags: true,
                    tokenSeparators: [',', ' '],
                    createTag: function (params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: '__'+term,
                            text: term,
                            newTag: true // add additional parameters
                        }
                    }
                });
                $(".btn-success").click(function(){
                    var html = $(".clone").html();
                    $(".increment").after(html);
                });

                $("body").on("click",".btn-danger",function(){
                    $(this).parents(".control-group").remove();
                });
            });
        </script>
    </div>

@endsection
