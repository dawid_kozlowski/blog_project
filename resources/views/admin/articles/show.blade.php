@extends('admin.layouts.app')

@section('content')
    <div class="content">
        <div class="container">
            <h1>{{ $article->name }}</h1>
            <h2>By: {{ $article->author->name }}</h2>
            <p>{!! $article->description !!}</p>
            <p>
                @if($article->tag)
                    @foreach($article->tag as $tag)
                        <a href="{{ route('articles_by_tag', ['id' => $tag->id]) }}">#{{ $tag->name }}</a>
                    @endforeach
                @endif
            </p>
{{--            TODO: move carousel to front--}}
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($gallery as $image)
                        <div class="carousel-item @if($loop->first) active @endif">
                            <img class="d-block w-100" src="{{ asset('img/' . $image->file_name . '.' . $image->file_type) }}" alt="slide" height="500px" width="100px">
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <a href="{{ action('Admin\ArticlesController@edit', $article->id) }}" class="btn btn-secondary">Edit</a>
            <form action="{{ action('Admin\ArticlesController@destroy', $article->id) }}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="btn btn-danger" value="Delete"/>
            </form>
        </div>
        <script>
            $(document).ready(function () {
                $('.carousel').carousel()
            })
        </script>
    </div>
@endsection
