@extends('admin.layouts.app')
@section('content')
    <div class="content">
        <div class="container">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Title</th>
                    <th scope="col">last update</th>
                    <th scope="col">active</th>
                    <th scope="col" colspan="2"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <th scope="row">{{ $article->id }}</th>
                            <td><a href="{{ route('show_article', ['id' => $article->id]) }}">{{ $article->name }}</a></td>
                            <td>{{ $article->updated_at->diffForHumans() }}</td>
                            <td>{{ $article->active }}</td>
                            <td><a href="{{ action('Admin\ArticlesController@edit', $article->id) }}" class="btn btn-secondary">EDIT</a>
{{--                                <a href="{{ action('Admin\ArticlesController@destroy', $article->id) }}" class="btn btn-danger">DELETE</a>--}}

                            </td>
                            <td>
                                <form action="{{ action('Admin\ArticlesController@destroy', $article->id) }}" method="POST">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $articles->links() }}
        </div>
    </div>
@endsection
