@extends('admin.layouts.app')

@section('content')
    <script src="https://cdn.ckeditor.com/ckeditor5/12.3.0/classic/ckeditor.js"></script>
    <script src="{{ asset('assets/select-2/js/select2.js') }}"></script>

    <div class="card uper">
        <div class="card-header">
            Edit Article
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('update_article', $article->id) }}" enctype="multipart/form-data">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="article_name">Title:</label>
                    <input type="text" class="form-control" name="article_name" value="{{ $article->name }}" />
                </div>
                <div class="form-group">
                    <label for="article_short_description">Short description:</label>
                    <input type="text" class="form-control" name="article_short_description" value="{{ $article->short_description }}" />
                </div>

                <div class="form-group">
                    <label for="article_description">Description:</label>
                    <textarea name="article_description" id="editor">{{ $article->description }}</textarea>
                </div>

{{--                Is Active--}}
                <div class="form-group">
                    <label for="article_active">Is active:</label>
                    <input type="checkbox" name="article_active" value="article_active"
{{--                           {{ ($article->active?'checked':'') }}--}}
                        @if($article->active) checked @endif>
                </div>

{{--                Image part--}}
                <div class="input-group control-group increment" >
                    <input type="file" name="image[]" id="fileToUpload">
                    <div class="input-group-btn">
                        <button class="btn btn-success" type="button"><i class="icon icon-plus"></i>Add Image</button>
                    </div>
                </div>

                <div class="clone hide" hidden>
                    <div class="control-group input-group" style="margin-top:10px">
                        <input type="file" name="image[]" id="fileToUpload">
                        <div class="input-group-btn">
                            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                        </div>
                    </div>
                </div>
                Select article main image:
                <div class="container form-inline">
                        @foreach($gallery as $img)
                            <div class="">
                                <div>
                                    <img src="{{ asset('thumbnail/'. $img->file_name . '.' . $img->file_type) }}" class="img-thumbnail" alt="Responsive image">
                                </div>
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="article_file" value="{{ $img->id }}"
                                           @if($activeImage)
                                                @if($img->id == $activeImage->id) checked @endif
                                           @endif>
                                    <label class="form-check-label" for="{{ $img->file_name }}">{{ $img->file_name }}</label>
                                    <button onclick="removeImg('{{ route('destroy_image', $img->id) }}'); return false;" class="btn btn-danger btn-xs btn-delete delete-img" value="{{ $img->id }}" type="button">Delete</button>
{{--                                    <a class="btn btn-danger deleteImage"--}}
{{--                                       data-id="{{ $article->id }}"--}}
{{--                                       data-token="{{ csrf_token() }}">Delete</a>--}}
{{--                                    <form  action="{{ action('Admin\GalleryController@destroy', $article->id) }}" method="POST">--}}
{{--                                        <input type="hidden" name="_method" value="DELETE">--}}
{{--                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                                        <input type="submit" class="btn btn-danger" value="Delete">--}}
{{--                                    </form>--}}
                                </div>
                            </div>
                        @endforeach
                </div>

{{--                Categories--}}
                <div class="form-group">
                    <label for="category_description">Slug URL:</label>
                    <input type="text" class="form-control" name="seo_url" value="@if($seoUrl != null){{ $seoUrl->slug }} @endif">
                </div>
                <div class="form-group">
                    <label for="article_category">Category:</label>
                    <select class="custom-select" name="article_category" id="article_category">
                        @foreach($allCategories as $category)
                            <option value="{{ $category->id }}"
                                    @if($category->id == $article->category->id)
                                        selected
                                    @endif>
                                {{ $category->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

{{--                Tags--}}
                <div class="form-group">
                    <label for="article_tags">Tags:</label>
                    <select class="custom-select" name="article_tags[]" id="article_tags" multiple>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}"
                                @foreach($article->tag as $atag)
                                    @if($atag->id == $tag->id)
                                        selected
                                    @endif
                                @endforeach
                            >#{{ $tag->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('all_articles') }}" class="btn btn-outline-info">Back to all</a>
            </form>
        </div>

        <script>
            $(document).ready(function () {
                ClassicEditor
                    .create( document.querySelector( '#editor' ) )
                    .catch( error => {
                        console.error( error );
                    } );
                $("#article_tags").select2({
                    tags: true,
                    tokenSeparators: [',', ' '],
                    createTag: function (params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: '__'+term,
                            text: term,
                            newTag: true // add additional parameters
                        }
                    }
                });
                $(".btn-success").click(function(){
                    var html = $(".clone").html();
                    $(".increment").after(html);
                });
                $("body").on("click",".btn-danger",function(){
                    $(this).parents(".control-group").remove();
                });



            });
            function removeImg(url) {
                let data = {
                    '_token': '{{ csrf_token() }}',
                    '_method': 'delete'
                };

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        console.log(data);

                        location.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
                return false;
            }
        </script>
    </div>
@endsection
