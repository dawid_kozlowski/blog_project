<div class="wrapper">
<!-- Sidebar -->
<nav id="sidebar">
    <ul class="list-unstyled components">
{{--        <p>Dummy Heading</p>--}}
        <li class="active">
{{--            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home</a>--}}
{{--            <ul class="collapse list-unstyled" id="homeSubmenu">--}}
{{--                <li>--}}
{{--                    <a href="">aaaaa</a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">Home 2</a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">Home 3</a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}
        <li>
            <a href="{{ route('all_articles') }}">All Articles</a>
        </li>
{{--        <li>--}}
{{--            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pages</a>--}}
{{--            <ul class="collapse list-unstyled" id="pageSubmenu">--}}
{{--                <li>--}}
{{--                    <a href="#">Page 1</a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">Page 2</a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">Page 3</a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}
        <li>
            <a href="{{ route('create_article') }}">Create Article</a>
        </li>
        <li>
            <a href="{{ route('categories') }}">Categories</a>
        </li>
        <li>
            <a href="{{ route('create_category') }}">Create category</a>
        </li>
    </ul>
</nav>
</div>

