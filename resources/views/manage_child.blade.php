
            @foreach($childs as $child)
                <div class="list-group-item subcat">
                    <a href="{{ route('articles_by_category', $child->id) }}">{{ $child->name }}</a>
                    @if(count($child->childs))
                        <a data-toggle="collapse" href="#collapse_{{ $child->id }}"><small><small><i class="glyphicon glyphicon-plus"></i></small></small></a>
                        <span class="badge">{{ count($child->childs) }}</span>
                    @endif
                </div>
                    @if(count($child->childs))
                        <div class="panel-collapse collapse" id="collapse_{{ $child->id }}">
                            <div class="panel-body">
                                @include('manage_child',['childs' => $child->childs])
                            </div>
                        </div>
                    @endif
            @endforeach



{{--old version--}}
{{--<ul>--}}
{{--    @foreach($childs as $child)--}}
{{--        <li>--}}
{{--            <a href="{{ route('articles_by_category', $child->id) }}">{{ $child->name }}</a>--}}
{{--            @if(count($child->childs))--}}
{{--                @include('manageChild',['childs' => $child->childs])--}}
{{--            @endif--}}
{{--        </li>--}}
{{--    @endforeach--}}
{{--</ul>--}}
