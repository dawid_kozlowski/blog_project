@extends('layouts.app')
@section('navbarOff')
    d-lg-block
@endsection
@section('content')
    <div class="p-2 col-lg-7 offset-lg-2 col-md-12 offset-md-0 justify-content-center col bd-highlight">
        <div class="content">
            <div class="container">
                @foreach($articles as $article)
                    @include('articles.article_card')
                @endforeach
            </div>
            {{ $articles->links('layouts.pagination') }}
        </div>
    </div>
@endsection

