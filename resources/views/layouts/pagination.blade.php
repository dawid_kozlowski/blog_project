<div class="ui right floated pagination menu pagination-menu">    <!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
        <a class="icon border item disabled">
            <i class="small glyphicon glyphicon-chevron-left"></i>
        </a>
    @else
        <a href="{{ $paginator->previousPageUrl() }}" class="icon border item">
            <i class="small glyphicon glyphicon-chevron-left"></i>
        </a>
    @endif
<!-- Pagination Elements -->
    @foreach ($elements as $element)
        @if (is_string($element))
        <a class="item">1</a>
        @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a class="border bg-main item active">{{ $page }}</a>
                    @else
                        <a class="border item" href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
    @endforeach
<!-- Next Page Link -->
    @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" class="icon border item"><i class="small glyphicon glyphicon-chevron-right"></i></a>
    @else
        <a class="icon border item disabled"><i class="small glyphicon glyphicon-chevron-right"></i></a>
    @endif
</div>
