<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
{{--    <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}
    <link href="https://fonts.googleapis.com/css?family=Lora&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
{{--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>--}}

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container-fluid px-0 justify-content-center">
            <div class="d-xl-none d-lg-none d-block">
                <a data-toggle="collapse" href="#collapse_menu"><i class="display-4 glyphicon glyphicon-menu-hamburger big"></i></a>
                <div class="panel-collapse collapse" id="collapse_menu">
                    @include('layouts.mobile_panel')
                </div>
            </div>
        <nav class="d-none navbar navbar-expand-md navbar-light main-navbar
            @yield('navbarOff')
            ">
            <div class="container">
                <div class="d-flex flex-column mb-3 ">
                    <div class="col-12 d-none d-lg-block">
                        <div class="justify-content-center p-2 bd-highlight">
                            <a class="text-center" href="{{ url('/') }}">
                                {{--                    {{ config('app.name', 'Blog') }}--}}
                                <h1 class="text-white font-8rem font-weight-bolder font-border">LORE IPSUM DOLOR</h1>
                                <hr class="dropdown-divider mb-5 mt-5 mr-n15 ml-n15">
                                <h1 class="text-white font-4rem font-weight-bold font-border">Sic Mundus Creatus Est</h1>

                            </a>
                        </div>
                    </div>
{{--                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">--}}
{{--                        <span class="navbar-toggler-icon"></span>--}}
{{--                    </button>--}}
{{--                    <div class="justify-content-end">--}}
{{--                        <div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
{{--                            <!-- Left Side Of Navbar -->--}}
{{--                            <ul class="navbar-nav mr-auto">--}}
{{--                                --}}{{--                        <a class="button" href="{{ route('articles.create') }}">New Article</a>--}}
{{--                            </ul>--}}

{{--                            <!-- Right Side Of Navbar -->--}}
{{--                            <ul class="navbar-nav ml-auto">--}}
{{--                                <!-- Authentication Links -->--}}
{{--                                @guest--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
{{--                                    </li>--}}
{{--                                    @if (Route::has('register'))--}}
{{--                                        <li class="nav-item">--}}
{{--                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
{{--                                        </li>--}}
{{--                                    @endif--}}
{{--                                @else--}}
{{--                                    <li class="nav-item dropdown">--}}
{{--                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
{{--                                            {{ Auth::user()->name }} <span class="caret"></span>--}}
{{--                                        </a>--}}
{{--                                        <a id="logout" class="dropdown-item" href="{{ route('logout') }}"--}}
{{--                                           onclick="event.preventDefault();--}}
{{--                                                     document.getElementById('logout-form').submit();">--}}
{{--                                            {{ __('Logout') }}--}}
{{--                                        </a>--}}
{{--                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
{{--                                            <a class="dropdown-item" href="{{ route('logout') }}"--}}
{{--                                               onclick="event.preventDefault();--}}
{{--                                                     document.getElementById('logout-form').submit();">--}}
{{--                                                {{ __('Logout') }}--}}
{{--                                            </a>--}}

{{--                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                                                @csrf--}}
{{--                                            </form>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                @endguest--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>

        </nav>
        <main class="py-4">
            <div class="d-flex flex-column">
                <div class="d-flex justify-content-around bd-highlight main-content flex-column-reverse flex-lg-row flex-xl-row">
                    @yield('content')
                    @include('layouts.right_panel')
                </div>
                <div class="order-last">
                    @section('footer')
                        Footer Footer Footer Footer FooterFooter Footer Footer Footer FooterFooter Footer Footer Footer FooterFooter Footer Footer Footer FooterFooter Footer Footer Footer FooterFooter Footer Footer Footer FooterFooter Footer Footer Footer FooterFooter Footer Footer Footer FooterFooter Footer Footer Footer FooterFooter Footer Footer Footer Footer
                    @show
                </div>
            </div>

        </main>

    </div>
    </div>
</body>
<script>
    $(function() {

        $('.list-group-item').on('click', function() {
            $('.glyphicon', this)
                .toggleClass('glyphicon-plus')
                .toggleClass('glyphicon-minus');
        });

    });
</script>
</html>
