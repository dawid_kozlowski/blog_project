<div class="p-6 col-lg-3 d-none d-lg-block bd-highlight mr-9">
    <div class="">
        <!-- Search form -->
        @include('layouts.search')
{{--        <input class="form-control poppins" type="text" placeholder="Search" aria-label="Search">--}}
    </div>
    <div>
        @include('categories')
    </div>
    <div>
        @include('articles.popular_articles')
    </div>
    <div>
        @include('tags.popular_tags')
    </div>
</div>
