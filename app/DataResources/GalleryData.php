<?php


namespace App\DataResources;


use Illuminate\Support\Facades\DB;

class GalleryData
{
    public function getActiveImage($id)
    {
        $data = DB::select(
            "SELECT * FROM galleries
            WHERE article_id = '$id'
            AND status = 'active'
            ");
        if (isset($data[0])) {
            return $data[0];
        }
        return null;
    }
}
