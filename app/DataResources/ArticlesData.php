<?php


namespace App\DataResources;

use Illuminate\Support\Facades\DB;

class ArticlesData
{
    public function getAll()
    {
        return DB::select(
            "SELECT *, a.name as article_name, u.name as author_name FROM articles a
            LEFT JOIN users u ON a.author_id = u.id
            ORDER BY a.created_at DESC
            ");
    }

    public function getOne($id)
    {
        $data = DB::select(
        "SELECT *, a.name as article_name, u.name as author_name, a.id as article_id FROM articles a
        LEFT JOIN users u ON a.author_id = u.id
        WHERE a.id = '$id'
        ");
        if (isset($data[0])) {
            return $data[0];
        }
        return null;

    }
}
