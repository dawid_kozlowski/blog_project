<?php


namespace App\DataResources;


use Illuminate\Support\Facades\DB;

class CategoriesData
{
    public function getAll()
    {
        $categories = DB::select("
            SELECT * FROM categories c
        ");

        return $categories;
    }

    public function getAllWithColumn(array $columns = ['id', 'name'])
    {
        return DB::select("
            SELECT 
                ".implode(", ", $columns) ."
            FROM categories c
        ");
    }

    public function getMain()
    {
        return DB::select("
            SELECT * FROM categories c
            WHERE c.parent_id = '0'
        ");
    }
}
