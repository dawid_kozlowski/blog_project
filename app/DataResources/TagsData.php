<?php


namespace App\DataResources;


use Illuminate\Support\Facades\DB;

class TagsData
{
    public function getByName()
    {
        return DB::select(
            'SELECT * FROM tags t
            ORDER BY t.name'
        );
    }

    public function getByArticle($id)
    {
        return DB::select(
            "SELECT * FROM tags t
            LEFT JOIN articles_to_tags att on t.id = att.tag_id
            WHERE att.article_id = '$id'
        ");
    }
}
