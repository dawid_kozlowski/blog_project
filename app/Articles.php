<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $fillable = ['name', 'description', 'short_description', 'author_id', 'category_id', 'active'];

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id');
    }

    public function tag()
    {
        return $this->belongsToMany('App\Tag', 'articles_to_tags', 'article_id', 'tag_id')->withTimestamps();
    }

    public function upadted_at()
    {

    }

    public function images()
    {
        return $this->hasMany('App\Gallery', 'article_id', 'id');
    }

    public function getLastUpdateDate()
    {
        return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    }

//    public function getSlugAttribute(): string
//    {
//        return str_slug($this->name);
//    }
//
//    public function getUrlAttribute(): string
//    {
//        return action('Admin\ArticlesController@detail', [$this->id, $this->slug]);
//    }
}
