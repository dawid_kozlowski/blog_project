<?php


namespace App\Http\ViewComposers;

use App\ArticlesToTags;
use App\Category;
use Illuminate\View\View;

class DataComposer
{
//    TODO: possible to remove
    public function __construct()
    {
        $categories = Category::where('parent_id', '=', 0)->get();

        $data = array(
            'categories' => $categories,
            'tags' => $this->popularTags()
        );
//        dd($data);
        return view('home', compact('data'));
    }

    private function popularTags()
    {
        $tags = ArticlesToTags::with('tag')->selectRaw('tag_id, count(tag_id) as total_articles')->groupBy('tag_id')->orderByDesc('total_articles')->take(10)->get();
        return $tags;
    }

    public function compose(View $view)
    {
        $view->with('home');
    }
}
