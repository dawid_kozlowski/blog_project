<?php

namespace App\Http\Controllers;

use App\Articles;
use App\ArticlesToTags;
use App\Category;
use App\DataResources\ArticlesData;
use App\DataResources\CategoriesData;
use App\DataResources\GalleryData;
use App\DataResources\TagsData;
use App\Gallery;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('11');
        $articles = Articles::orderBy('created_at', 'DESC')->paginate(15);
        return view('home', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $tags = Tag::orderBy('name')->get();
        $tagsData = new TagsData();
        $tags = $tagsData->getByName();
        $allCategories = Category::all();
        return view('articles.create', compact('allCategories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'article_name' => 'required',
            'article_short_description' => 'required',
            'article_description' => 'required',
            'article_category' => 'required'
        ]);

        $tags = $request->input('article_tags');

        $article = new Articles([
            'name' => $request->get('article_name'),
            'short_description' => $request->get('article_short_description'),
            'description' => $request->get('article_description'),
            'author_id' => Auth::user()->getAuthIdentifier(),
            'category_id' => $_POST['article_category']
        ]);

        $article->save();
        $article->tag()->attach($tags);
        return redirect('/articles')->with('success', 'Article has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

//        $image = Gallery::where('article_id', '=', $id)->where('status', '=', 'active')->first();
        $imageData = new GalleryData();
        $image = $imageData->getActiveImage($id);
        $gallery = Gallery::where('article_id', $id)->get();
//        dd($image);
//        dd($image->file_name);
//        $article = Articles::find($id);
        $articlesData = new ArticlesData();
        $article = $articlesData->getOne($id);
        $tagsData = new TagsData();
        $tags = collect($tagsData->getByArticle($id));
        return view('articles.show', compact('article', 'image', 'tags', 'gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allCategories = Category::all();
//        $categoryData = new CategoriesData();
//        $allCategories = collect($categoryData->getAll());
//        $article = Articles::find($id);
        $articlesData = new ArticlesData();
        $article = $articlesData->getOne($id);

        $tagsData = new TagsData();
        $article_tags = collect($tagsData->getByArticle($id));

        $tagsData = new TagsData();
        $tags = $tagsData->getByName();

        return view('articles.edit', compact('article', 'allCategories', 'tags', 'article_tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'article_name' => 'required',
            'article_short_description' => 'required',
            'article_description' => 'required'
        ]);

        $article = Articles::find($id);

        $article->tag()->detach();

        $tags = $request->input('article_tags');
        $article->tag()->attach($tags);
        $article->name = $request->get('article_name');
        $article->short_description = $request->get('article_short_description');
        $article->description = $request->get('article_description');
        $article->category_id = $_POST['article_category'];

        $article->save();
        return redirect()->route('articles.show', compact('article'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Articles::find($id);
        $article->tag()->detach();
        $article->delete();
        return redirect('/articles')->with('success', 'Article has been deleted Successfully');
    }
//TODO move from CategoryController
    public function showByCategory($id)
    {
        $cat = new Category();
        $ids = array_unique($cat->getChildsIds($id));

        $articles = Articles::whereIn('category_id', $ids)->where('active', '=', '1')->orderBy('updated_at', 'DESC')->paginate(10);

//        dd($articles);
        return view('articles', compact('articles'));
    }
//TODO Move from
    public function showByTag($id)
    {
//        dd(1111);
        $articles = Articles::whereHas('tag', function ($q) use($id) {
            $q->where('tag_id', $id);
        })->orderBy('updated_at', 'DESC')->paginate(10);
        return view('articles', compact('articles'));
    }

    public function search(Request $request)
    {
        $q = $request->get('q');
        $articles = Articles::where('name','LIKE','%'.$q.'%')->orWhere('short_description','LIKE','%'.$q.'%')->paginate(10);
        if(count($articles) > 0)
            return view('search_home', compact('articles'));
        else return view ('welcome')->withMessage('No Details found. Try to search again !');
    }
}
