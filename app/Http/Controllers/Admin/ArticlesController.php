<?php

namespace App\Http\Controllers\Admin;

use App\Articles;
use App\Category;
use App\Gallery;
use App\SeoUrl;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;
//require 'vendor/autoload.php';

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Articles::orderBy('updated_at', 'DESC')->paginate(15);
//        dump($articles);
        return view('admin.articles.index', compact('articles'));
    }

    public function edit($id)
    {
        $seoUrl = SeoUrl::where('URL', '/articles/'.$id)->first();
        $gallery = Gallery::where('article_id', $id)->get();
        $activeImage = $gallery->where('status', 'active')->first();

        $allCategories = Category::all();
        $article = Articles::find($id);
        $tags = Tag::orderBy('name')->get();
        return view('admin.articles.edit',
            compact('article', 'allCategories', 'tags' , 'gallery', 'activeImage', 'seoUrl'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'article_name' => 'required',
            'article_short_description' => 'required',
            'article_description' => 'required'
        ]);

        $article = Articles::find($id);

        $article->tag()->detach();
        $tags = $request->input('article_tags');

        if ($tags)
        {
            $new_tags =[];
            foreach ($tags as $key => $tag) {
                if(substr($tag,0,2) == '__') {

                    $add_tag = new Tag();
                    $add_tag->name = substr($tag, 2);
                    $add_tag->save();

                    $new_tags[] = $add_tag->id;
                    unset($tags[$key]);
                }
            }
            $article->tag()->attach($new_tags);
        }
        $images = Gallery::where('article_id', $id)->get();
        foreach($images as $image){
            if($image->id == $request->get('article_file')){
                $image->status = 'active';
                $image->save();
            } else {
                $image->status = '0';
                $image->save();
            }
        }


        $article->tag()->attach($tags);
        $article->name = $request->get('article_name');
        $article->short_description = $request->get('article_short_description');
        $article->description = $request->get('article_description');
        $article->active = (($request->has('article_active'))? '1' : '0');
        $article->category_id = $_POST['article_category'];

        $article->save();
        $store = new Gallery();
        if ($request->file('image')){
            $store->storeImages($request->file('image'), $article);
        }

        if ($request->get('seo_url')) {
            SeoUrl::where('URL' ,'=', route('articles.show', ['article' => $article->id], false))->delete();
            $seoUrl = new SeoUrl();
            $seoUrl->url = route('articles.show', ['article' => $article->id], false);
            $seoUrl->slug = $request->get('seo_url');
            $seoUrl->save();
        }

//        return redirect('articles.show')->with('success', 'Article has been updated');
        return redirect()->route('show_article', compact('article'));
    }

    public function show($id)
    {
        $article = Articles::find($id);
        $gallery = Gallery::where('article_id', $id)->get();
        return view('admin.articles.show', compact('article', 'gallery'));
    }
    public function destroy($id)
    {
        $article = Articles::find($id);
        if (isset($article->images)){
            foreach($article->images as $image) {
                /** @var Gallery $image */
                $image->deleteImage();
            }
        }
        if (isset($article->tag)) {
            $article->tag()->detach();
        }
        $article->delete();
        return redirect()->route('all_articles')->with('success', 'Article has been deleted Successfully');
    }

    public function store(Request $request)
    {
        $request->validate([
            'article_name' => 'required',
            'article_short_description' => 'required',
            'article_description' => 'required',
            'article_category' => 'required'
        ]);

        $tags = $request->input('article_tags');

        $new_tags =[];
        if ($tags){
            foreach ($tags as $key => $tag) {
                if(substr($tag,0,2) == '__') {

                    $add_tag = new Tag();
                    $add_tag->name = substr($tag, 2);
                    $add_tag->save();

                    $new_tags[] = $add_tag->id;
                    unset($tags[$key]);
                }
            }
        }


        $article = new Articles([
            'name' => $request->get('article_name'),
            'short_description' => $request->get('article_short_description'),
            'description' => $request->get('article_description'),
            'active' => (($request->has('article_active'))? '1' : '0'),
            'author_id' => Auth::user()->getAuthIdentifier(),
            'category_id' => $_POST['article_category']
        ]);


        $article->save();
        $article->tag()->attach($tags);
        $article->tag()->attach($new_tags);

        $store = new Gallery();
        if ($request->file('image')){
            $store->storeImages($request->file('image'), $article);
        }

        return redirect()->route('show_article', compact('article'));
    }

    public function create()
    {
        $tags = Tag::orderBy('name')->get();
        $allCategories = Category::all();
        return view('admin.articles.create', compact('allCategories', 'tags'));
    }

//    public function detail($id)
//    {
//        return view('admin.articles.show')->withArticle(Articles::findOrFail($id));
//    }
}
