<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\SeoUrl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id', '=', 0)->get();

        return view('admin.categories.categories', compact('categories'));
    }

    public function edit($id)
    {
        $seoUrl = SeoUrl::where('URL', '/categories/'.$id)->first();
        $allCategories = Category::all();
        $category = Category::find($id);
        return view('admin.categories.edit', compact('allCategories', 'category', 'seoUrl'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'category_name' => 'required',
            'category_description' => 'required'
        ]);



        $category = Category::find($id);

        $category->name = $request->get('category_name');
        $category->description = $request->get('category_description');
        $category->active = (($request->has('category_active'))? '1' : '0');
        $category->parent_id = $_POST['parent_category'];

        $category->save();

//        TODO: to be separated
        if ($request->get('seo_url')) {
            SeoUrl::where('URL' ,'=', route('articles_by_category', ['id' => $category->id], false))->delete();
            $seoUrl = new SeoUrl();
            $seoUrl->url = route('articles_by_category', ['id' => $category->id], false);
            $seoUrl->slug = $request->get('seo_url');

            $seoUrl->save();
        }

        return redirect()->route('categories');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect()->route('categories')->with('success', 'Category has been deleted Successfully');
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_name' => 'required',
            'category_description' => 'required'
        ]);

        $category = new Category([
           'name' => $request->get('category_name'),
           'description' =>$request->get('category_description'),
           'parent_id' => $_POST['parent_category']
        ]);
        $category->save();
        return redirect()->route('categories');
    }

    public function create()
    {
        $allCategories = Category::all();

        return view('admin.categories.create', compact('allCategories'));
    }
}
