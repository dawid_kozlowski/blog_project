<?php

namespace App\Http\Controllers;

use App\Articles;
use App\Category;
use App\DataResources\ArticlesData;
use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $image = Gallery::where('article_id', '=', $id)->where('status', '=', 'active')->first();
        $articles = Articles::orderBy('created_at', 'DESC')->paginate(15);
//        $articlesData = new ArticlesData();
//        $articles = collect($articlesData->getAll());

        return view('home', compact('articles'));
    }
}
