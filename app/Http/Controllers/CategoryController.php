<?php

namespace App\Http\Controllers;

use App\Articles;
use App\Category;
use App\DataResources\CategoriesData;
use App\Gallery;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function manageCategory()
    {
        $categoriesData = new CategoriesData();
        //    TODO: probably unnecessary
//        $categories = Category::where('parent_id', '=', 0)->get();
        $categories = $categoriesData->getMain();
//        $allCategories = Category::pluck('name', 'id')->all();
        $allCategories = $categoriesData->getAllWithColumn(['name', 'id']);

        return view('categories', compact('categories', 'allCategories'));
    }
}
