<?php

namespace App\Http\Controllers;

use App\Articles;
use App\ArticlesToTags;
use App\Category;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $categories = Category::where('parent_id', '=', 0)->where('active', '=', '1')->get();
        $popularArticles = Articles::orderBy('updated_at', 'DESC')->take(3)->get();

        $data = array(
            'categories' => $categories,
            'tags' => $this->popularTags(),
            'popularArticles' => $popularArticles
        );
//        dd($data);
        view()->share('data', $data);
    }

    private function popularTags()
    {
        $tags = ArticlesToTags::with('tag')->selectRaw('tag_id, count(tag_id) as total_articles')->groupBy('tag_id')->orderByDesc('total_articles')->take(10)->get();
        return $tags;
    }
}
