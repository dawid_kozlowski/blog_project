<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class Gallery extends Model
{
    protected $fillable = ['file_name', 'file_type', 'status', 'article_id'];

    public function deleteImage()
    {
        if($this->id)
        {
            if (file_exists(public_path('/img/' . $this->file_name . '.' . $this->file_type)))
            {
                unlink(public_path('/img/' . $this->file_name . '.' . $this->file_type));
            }
            if (file_exists(public_path('/thumbnail/' . $this->file_name . '.' . $this->file_type)))
            {
                unlink(public_path('/thumbnail/' . $this->file_name . '.' . $this->file_type));
            }
            $this->delete();
        }
    }

    public function storeImages($images, $article)
    {
        $i = 0;
        foreach ($images as $image)
        {
            $imageType = $image->getClientOriginalExtension();
            $imageN = time() . '_' . $i;
            $imageName = $imageN . '.' . $imageType;

            $img = new Gallery([
                'file_name' => $imageN,
                'file_type' => $imageType,
                'article_id' => $article->id
            ]);
            $img->save();

            $i++;
            $image->move(base_path() . '/public/img/', $imageName);
            $this->createThumbnail($imageName);
        }
    }

    private function createThumbnail($imageName, $x = 300, $y = null)
    {
        copy(base_path() . '/public/img/' . $imageName, base_path() . '/public/thumbnail/' . $imageName);
        $thumbnailImg = Image::make(base_path() . '/public/thumbnail/' . $imageName)
            ->resize($x, $y, function ($constraint) {
                $constraint->aspectRatio();
            })->fit($x);
        $thumbnailImg->save(base_path() . '/public/thumbnail/' . $imageName);
    }
}
