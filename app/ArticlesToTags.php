<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticlesToTags extends Model
{

    protected $fillable = ['tag_id', 'article_id'];

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id', 'id');
    }
}
