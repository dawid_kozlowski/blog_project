<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'description', 'parent_id'];
    private $ids = array();

    public function childs()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id');
    }

//
    public function getChildsIds($id)
    {
        array_push($this->ids, (int)$id);
        $childs = Category::where('parent_id', $id)->get();
        if ($childs != null) {
            foreach ($childs as $child) {
                array_push($this->ids, $child->id);
                $this->getChildsIds($child->id);
            }
        }
        return $this->ids;
    }
}
