<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Articles;
use App\Model;
use App\Tag;
use Faker\Generator as Faker;

$factory->define(App\ArticlesToTags::class, function (Faker $faker) {
    return [
        'tag_id' => $faker->randomElement(Tag::pluck('id', 'id')->toArray()),
        'article_id' => $faker->randomElement(Articles::pluck('id', 'id')->toArray())
    ];
});
