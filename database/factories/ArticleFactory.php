<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\User;
use Faker\Generator as Faker;


$factory->define(App\Articles::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence('3'),
        'description' => $faker->paragraph,
        'short_description' => $faker->sentence('10'),
        'active' => $faker->randomElement(['0', '1']),
        'category_id' => $faker->randomElement(Category::pluck('id', 'id')->toArray()),
        'author_id' => $faker->randomElement(User::pluck('id', 'id')->toArray()),
    ];
});

