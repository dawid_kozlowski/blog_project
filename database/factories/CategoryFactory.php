<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;


$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence(2),
        'active' => $faker->randomElement(['0', '1']),
        'parent_id' => $faker->randomElement($array = array(0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8))
    ];
});
