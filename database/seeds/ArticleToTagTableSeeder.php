<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ArticleToTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ArticlesToTags::class, 100)->create();
    }
}
