<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        TODO: Category seeder needs few improvements
        $this->call(CategoriesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ArticlesSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(ArticleToTagTableSeeder::class);
    }
}
