<?php

use Illuminate\Database\Seeder;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Articles::class)->create();
    }

//    private function getRandomUserId()
//    {
//        $user = App\User::class;
//    }
}
